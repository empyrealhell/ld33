﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Domain
{
    public interface IPointEvent
    {
        string Name { get; }

        int Value { get; }

        GameObject Originator { get; }

        bool Excludes(IPointEvent otherEvent);
    }

    public class DestructionPointEvent : IPointEvent
    {
        public string Name { get; private set; }

        public int Value { get; private set; }

        public GameObject Originator { get; private set; }

        public DestructionPointEvent(string name, int value, GameObject originator)
        {
            this.Name = name;
            this.Value = value;
            this.Originator = originator;
        }

        public bool Excludes(IPointEvent otherEvent)
        {
            return otherEvent.GetType().Equals(this.GetType())
                && otherEvent.Originator.Equals(this.Originator);
        }
    }

    public class AreaPointEvent : IPointEvent
    {
        public string Name { get; private set; }

        public int Value { get; private set; }

        public GameObject Originator { get; private set; }

        public GameObject Area { get; private set; }

        public AreaPointEvent(string name, int value, GameObject originator, GameObject area)
        {
            this.Name = name;
            this.Value = value;
            this.Originator = originator;
            this.Area = area;
        }

        public bool Excludes(IPointEvent otherEvent)
        {
            return otherEvent.GetType().Equals(this.GetType())
                && otherEvent.Originator.Equals(this.Originator)
                && (otherEvent as AreaPointEvent).Area.Equals(this.Area);
        }
    }

    public class Combo
    {
        private float timeLastUpdated;

        private List<IPointEvent> pointEvents;

        public Combo()
        {
            this.pointEvents = new List<IPointEvent>();
        }

        public bool AddPointEvent(IPointEvent newPointEvent)
        {
            var otherEvents = this.pointEvents.Where(a => a.Excludes(newPointEvent)).ToList();

            if (otherEvents.Count == 0)
            {
                this.pointEvents.Add(newPointEvent);
                this.timeLastUpdated = Time.realtimeSinceStartup;
                GameObject.FindObjectOfType<AudioManager>().PlayCombo(this.pointEvents.Count);
                return true;
            }

            return false;
        }

        public int GetScore()
        {
            int value = 0;
            foreach (var pointEvent in this.pointEvents)
                value += pointEvent.Value;
            value *= this.pointEvents.Count;
            return value;
        }

        public float GetTimeSinceLastUpdate()
        {
            return (Time.realtimeSinceStartup - this.timeLastUpdated);
        }

        public float GetComboRemaining()
        {
            return 1f - ((Time.realtimeSinceStartup - this.timeLastUpdated) / this.GetComboTime());
        }

        public int GetComboSize()
        {
            return this.pointEvents.Count;
        }

        public float GetComboTime()
        {
            return Mathf.Clamp(5f - 0.25f * this.pointEvents.Count, 2.5f, 5f);
        }

        public bool IsActive()
        {
            return (Time.realtimeSinceStartup - this.timeLastUpdated) < this.GetComboTime();
        }

        public IPointEvent GetLatestEvent()
        {
            return this.pointEvents[this.pointEvents.Count - 1];
        }
    }
}
