﻿using UnityEngine;
using System.Collections;

public class AreaCollider : MonoBehaviour
{
    public int PlayerPointValue = 50;

    public string PlayerEnterText;

    public int ObjectPointValue = 100;

    public string ObjectEnterText;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.GetComponent<CollidableObject>() != null)
            GameObject.FindObjectOfType<ScoreManager>().AddPointEvent(this.ObjectEnterText, this.ObjectPointValue, collider.gameObject, this.gameObject);
        else
            GameObject.FindObjectOfType<ScoreManager>().AddPointEvent(this.PlayerEnterText, this.PlayerPointValue, collider.gameObject, this.gameObject);
    }
}
