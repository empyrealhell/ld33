﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;

public enum GameState
{
    Menu,
    Ready,
    Playing
}

public class StateManager : MonoBehaviour
{
    public GameState State = GameState.Menu;

    public int SessionLength;

    public List<GameObject> MenuUI;

    public List<GameObject> PlayingUI;

    public Text ReadyCounter;

    public Text PlayingCounter;

    public Text HighScore;

    private float stateChangeTime;

    private static string highScoreKey = "HighScore";

    private bool played = false;

    private int lastNumber = 0;

	// Use this for initialization
	void Start ()
    {
        this.ChangeState(GameState.Menu);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (this.State == GameState.Menu)
        {
            this.HighScore.text = PlayerPrefs.GetInt(highScoreKey).ToString("N0");
            var currentScale = this.HighScore.transform.localScale;
            currentScale.x = 1.0f + Easing.Bounce(0.1f, 3.0f, Time.realtimeSinceStartup);
            currentScale.y = 1.0f + Easing.Bounce(0.1f, 3.0f, 1.5f + Time.realtimeSinceStartup);
            this.HighScore.transform.localScale = currentScale;

            currentScale = this.MenuUI[1].transform.localScale;
            currentScale.x = currentScale.y = 1.0f + Easing.Pop(0.05f, 1.0f, Time.realtimeSinceStartup);
            this.MenuUI[1].transform.localScale = currentScale;

            if (Input.GetAxis("Reset") != 0f)
            {
                this.ChangeState(GameState.Ready);
            }
        }
        else if (this.State == GameState.Ready)
        {
            int countdown = Mathf.CeilToInt(3 - (Time.realtimeSinceStartup - this.stateChangeTime));
            if (lastNumber != countdown)
            {
                GameObject.FindObjectOfType<AudioManager>().PlayTick();
                this.lastNumber = countdown;
            }

            var currentScale = this.ReadyCounter.transform.localScale;
            currentScale.x = currentScale.y = 1.0f + Easing.Pop(0.25f, 1.0f, Time.realtimeSinceStartup - this.stateChangeTime);
            this.ReadyCounter.transform.localScale = currentScale;

            this.ReadyCounter.text = countdown.ToString();
            if (countdown <= 0)
            {
                this.ReadyCounter.text = "GO!";
                this.ChangeState(GameState.Playing);
            }
        }
        else if (this.State == GameState.Playing)
        {
            float time = this.GetTimeRemaining();
            if (time < this.SessionLength - 3)
            {
                this.ReadyCounter.enabled = false;
            }
            else
            {
                var currentScale = this.ReadyCounter.transform.localScale;
                currentScale.x = currentScale.y = 1.0f + Easing.Bounce(0.25f, 1.0f, Time.realtimeSinceStartup - this.stateChangeTime);
                this.ReadyCounter.transform.localScale = currentScale;
            }
            this.PlayingCounter.text = time.ToString("N2");

            if (time < 30)
            {
                this.PlayingCounter.color = new Color(Mathf.Lerp(0f, 1f, (30 - time) / 7f), Mathf.Lerp(1f, 0f, (30 - time) / 7f), 0f);

                float progress = Mathf.Lerp(24f, 72f, (30 - time) / 15f);
                this.PlayingCounter.fontSize = (int) progress;
            }

            if (time <= 0)
            {
                var manager = GameObject.FindObjectOfType<ScoreManager>();
                if (manager.GetScore() > PlayerPrefs.GetInt(highScoreKey))
                {
                    PlayerPrefs.SetInt(highScoreKey, manager.GetScore());
                }
                this.ChangeState(GameState.Menu);
            }
        }
    }

    public void ChangeState(GameState state)
    {
        this.stateChangeTime = Time.realtimeSinceStartup;
        this.State = state;
        if (state == GameState.Menu)
        {
            GameObject.FindObjectOfType<AudioManager>().PlayMenu();

            foreach (var item in this.PlayingUI)
                item.SetActive(false);

            foreach (var item in this.MenuUI)
                item.SetActive(true);

            this.ReadyCounter.enabled = false;
            this.PlayingCounter.enabled = false;
            this.HighScore.enabled = true;
        }
        else if (state == GameState.Ready)
        {
            GameObject.FindObjectOfType<AudioManager>().Stop();
            if (this.played)
                Application.LoadLevel(0);
            foreach (var item in this.PlayingUI)
                item.SetActive(true);

            foreach (var item in this.MenuUI)
                item.SetActive(false);

            this.ReadyCounter.enabled = true;
            this.PlayingCounter.enabled = true;
            this.HighScore.enabled = false;
        }
        else if (state == GameState.Playing)
        {
            this.played = true;
            GameObject.FindObjectOfType<AudioManager>().PlayTheme();
            GameObject.FindObjectOfType<AudioManager>().PlayTick();

            foreach (var item in this.PlayingUI)
                item.SetActive(true);

            foreach (var item in this.MenuUI)
                item.SetActive(false);

            this.ReadyCounter.enabled = true;
            this.PlayingCounter.enabled = true;
            this.HighScore.enabled = false;
        }
    }

    public float GetTimeRemaining()
    {
        return this.SessionLength - (Time.realtimeSinceStartup - this.stateChangeTime);
    }
}
