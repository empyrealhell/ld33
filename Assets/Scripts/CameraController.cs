﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public GameObject Target;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.LookAt(Target.transform);
	}
}
