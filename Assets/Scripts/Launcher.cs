﻿using UnityEngine;
using System.Collections;

public class Launcher : MonoBehaviour
{
    public Vector3 Force;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        var body = collider.GetComponent<Rigidbody>();
        if (body)
        {
            body.AddForce(this.Force);
        }
    }

    void OnTriggerStay(Collider collider)
    {
        var body = collider.GetComponent<Rigidbody>();
        if (!body)
            body = collider.GetComponentInParent<Rigidbody>();
        if (body)
            body.AddForce(this.Force * body.mass);
    }
}
