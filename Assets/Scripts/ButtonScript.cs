﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour
{
    public GameObject Activate;

	// Use this for initialization
	void Start ()
    {
        this.Activate.SetActive(false);
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnCollisionEnter(Collision collider)
    {
        this.Activate.SetActive(true);
    }
}
