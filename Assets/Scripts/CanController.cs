﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Scripts;

public class CanController : MonoBehaviour
{
    public GameObject CanBody;
    public float MaxTorque;
    public ParticleSystem ParticleSystem;
    public GameObject RespawnPoint;

    public Image FizzMeter;

    private Vector3 leftSide;
    private Vector3 rightSide;

    private Vector3 lastDirection;
    private Vector3 lastVelocity;

    private float lastRotation;
    private float lastAcceleration;

    private float Fizz;

    // Use this for initialization
    void Start ()
    {
        this.GetComponent<Rigidbody>().maxAngularVelocity = 25f;

        this.ParticleSystem.enableEmission = false;
        float size = this.CanBody.transform.localScale.y;
        leftSide = new Vector3(0.0f, size - 0.1f, 0.0f);
        rightSide = new Vector3(0.0f, -(size - 0.1f), 0.0f);

        this.lastDirection = Vector3.up;
        this.lastVelocity = Vector3.up;
        this.Fizz = 1f;
    }

    private void ResetObject()
    {
        var resetPoint = this.RespawnPoint.transform.position;

        var position = this.transform.position;
        position.Set(resetPoint.x, resetPoint.y, resetPoint.z);
        this.transform.position = position;

        var rotation = this.transform.rotation;
        var angles = rotation.eulerAngles;
        angles.Set(0, 0, 0);
        rotation.eulerAngles = angles;
        this.transform.rotation = rotation;

        var body = this.GetComponent<Rigidbody>();
        var velocity = body.velocity;
        velocity.Set(0, 0, 0);
        body.velocity = velocity;

        var angularVelocity = body.angularVelocity;
        angularVelocity.Set(0, 0, 0);
        body.angularVelocity = angularVelocity;
    }

    // Update is called once per frame
    void Update ()
    {
	
	}

    void FixedUpdate()
    {
        if (GameObject.FindObjectOfType<StateManager>().State != GameState.Playing)
            return;

        float left = Input.GetAxis("Left");
        float right = Input.GetAxis("Right");

        var direction = Vector3.Cross(transform.up, Vector3.up);
        var leftForce = direction * left * MaxTorque;
        var rightForce = direction * right * MaxTorque;
        var body = this.GetComponent<Rigidbody>();

        var leftPoint = transform.TransformPoint(leftSide);
        var rightPoint = transform.TransformPoint(rightSide);

        Debug.DrawRay(leftPoint, leftForce, Color.red);

        RaycastHit hitData;
        if (Physics.Raycast(leftPoint, Vector3.down, out hitData, 1.0f))
            body.AddForceAtPosition(leftForce, leftPoint);
        if (Physics.Raycast(rightPoint, Vector3.down, out hitData, 1.0f))
            body.AddForceAtPosition(rightForce, rightPoint);

        float angle = Vector3.Angle(this.lastDirection, direction);
        float accel = this.GetComponent<Rigidbody>().velocity.magnitude - this.lastVelocity.magnitude;
        this.lastDirection = direction;
        this.lastVelocity = this.GetComponent<Rigidbody>().velocity;

        if (Input.GetAxis("Spray") != 0f && this.Fizz > 0.05f)
        {
            GameObject.FindObjectOfType<AudioManager>().PlaySpray();
            Vector3 sprayAngle = new Vector3(transform.up.x, transform.up.y, transform.up.z);
            sprayAngle = Vector3.RotateTowards(sprayAngle, transform.right, Mathf.PI / 4f, 1f);
            this.ParticleSystem.transform.rotation = Quaternion.LookRotation(sprayAngle);
            this.ParticleSystem.enableEmission = true;
            this.ParticleSystem.startSpeed = Mathf.Log10(this.Fizz * 9f + 1f) * 25f;
            Debug.DrawRay(leftPoint, sprayAngle * 10, Color.green);
            Debug.DrawRay(rightPoint, -sprayAngle * 10, Color.blue);
            body.AddForceAtPosition(leftPoint, sprayAngle * -(MaxTorque * 1.5f) * Mathf.Log10(this.Fizz * 9f + 1f));
            body.AddForceAtPosition(rightPoint, sprayAngle * (MaxTorque * 1.5f) * Mathf.Log10(this.Fizz * 9f + 1f));
            this.Fizz -= 0.01f;
        }
        else
        {
            this.ParticleSystem.enableEmission = false;
            this.Fizz += (Mathf.Abs((angle - this.lastRotation) / 72f) + Mathf.Abs((accel - this.lastAcceleration) / 10f)) / 10f;
            this.lastRotation = angle;
            this.lastAcceleration = accel;
        }
        this.Fizz = Mathf.Clamp(this.Fizz, 0f, 1f);

        if (this.FizzMeter)
        {
            this.FizzMeter.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 200f * this.Fizz);
            if (this.Fizz >= 1f)
            {
                var currentScale = this.FizzMeter.transform.localScale;
                currentScale.x = currentScale.y = 1.1f + Easing.Bounce(0.1f, 1f, Time.realtimeSinceStartup);
                this.FizzMeter.transform.localScale = currentScale;
                var color = new Color(0.5f + Easing.Bounce(0.5f, 1f, Time.realtimeSinceStartup), 1f, 0f);
                this.FizzMeter.color = color;
            }
            else
            {
                var currentScale = this.FizzMeter.transform.localScale;
                currentScale.x = currentScale.y = 1f;
                this.FizzMeter.transform.localScale = currentScale;
                this.FizzMeter.color = Color.green;
            }
        }

        if (Input.GetAxis("Reset") != 0f)
            this.ResetObject();
    }
}