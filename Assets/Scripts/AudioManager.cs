﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public AudioClip Menu;

    public AudioClip Theme;

    public List<AudioClip> Aluminum;

    public List<AudioClip> Cardboard;

    public List<AudioClip> Combo;

    public List<int> ComboValues;

    public AudioClip Tick;

    public List<AudioClip> Spray;
    private AudioClip lastSpray;
    private float lastSprayTime;

    private System.Random random = new System.Random();
    private AudioSource Source;

    public void PlayMenu()
    {
        this.Source = this.GetComponent<AudioSource>();
        this.Source.clip = this.Menu;
        this.Source.loop = true;
        this.Source.Play();
    }

    public void PlayTheme()
    {
        this.Source = this.GetComponent<AudioSource>();
        this.Source.clip = this.Theme;
        this.Source.loop = false;
        this.Source.Play();
    }

    public void Stop()
    {
        this.Source.Stop();
    }

    public void PlayTick()
    {
        this.Source.PlayOneShot(this.Tick);
    }

    // Use this for initialization
    void Start ()
    {
        this.PlayMenu();
    }

    // Update is called once per frame
    void Update ()
    {

	}

    public void PingAluminum()
    {
        this.Source.PlayOneShot(this.Aluminum[this.random.Next(this.Aluminum.Count)]);
    }

    public void PingCardboard()
    {
        this.Source.PlayOneShot(this.Cardboard[this.random.Next(this.Cardboard.Count)]);
    }

    public void PlayCombo(int value)
    {
        int index = this.ComboValues.IndexOf(value);
        if (index >= 0)
            this.Source.PlayOneShot(this.Combo[index], 1.0f);
    }

    public void PlaySpray()
    {
        if (this.lastSpray == null || (Time.realtimeSinceStartup - this.lastSprayTime > this.lastSpray.length))
        {
            this.lastSpray = this.Spray[this.random.Next(this.Spray.Count)];
            this.lastSprayTime = Time.realtimeSinceStartup;
            this.Source.PlayOneShot(this.lastSpray);
        }
    }
}
