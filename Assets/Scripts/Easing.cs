﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public static class Easing
    {
        public static float Bounce(float magnitude, float period, float time)
        {
            float value = (time - Mathf.Floor(time / period) * period) / period;
            return magnitude * Mathf.Sin(value * Mathf.PI * 2f);
        }

        public static float Pop(float magnitude, float period, float time)
        {
            float value = (time - Mathf.Floor(time / period) * period) / period;
            return magnitude * Mathf.Clamp(Mathf.Sin(value * Mathf.PI * 2f), 0f, 1f);
        }
    }
}
