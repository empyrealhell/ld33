﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Domain;
using Assets.Scripts;

public class ScoreManager : MonoBehaviour
{
    public Text ScoreDisplay;

    public Text ComboDisplay;

    public Text ComboScore;

    public Image ComboBar;
    private float barWidth;

    private int score;

    private Combo currentCombo = null;

    private float comboEnd;

	// Use this for initialization
	void Start ()
    {
        this.barWidth = this.ComboBar.rectTransform.rect.width;
        this.ComboBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (this.currentCombo != null)
        {
            if (this.currentCombo.IsActive())
            {
                float comboTime = currentCombo.GetTimeSinceLastUpdate();

                if (comboTime < 1f)
                {
                    var currentScale = this.ComboScore.transform.localScale;
                    currentScale.x = 1.0f + Easing.Pop(0.25f, 1.0f, comboTime);
                    currentScale.y = 1.0f + Easing.Pop(0.25f, 1.0f, comboTime);
                    this.ComboScore.transform.localScale = currentScale;

                    currentScale = this.ComboDisplay.transform.localScale;
                    currentScale.x = 1.0f + Easing.Pop(0.25f, 1.0f, comboTime);
                    currentScale.y = 1.0f + Easing.Pop(0.25f, 1.0f, comboTime);
                    this.ComboDisplay.transform.localScale = currentScale;
                }

                this.ComboScore.text = this.currentCombo.GetScore().ToString("N0");
                this.ComboDisplay.text = string.Format("x{2} {0} ({1})", this.currentCombo.GetLatestEvent().Name, this.currentCombo.GetLatestEvent().Value, this.currentCombo.GetComboSize());
                this.ComboBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, this.currentCombo.GetComboRemaining() * this.barWidth);
            }
            else
            {
                this.comboEnd = Time.realtimeSinceStartup;
                this.score += this.currentCombo.GetScore();
                this.currentCombo = null;
                this.ScoreDisplay.text = this.score.ToString("N0");
                this.ComboScore.text = "";
                this.ComboDisplay.text = "";
                this.ComboBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
            }
        }
        var comboEndTime = Time.realtimeSinceStartup - this.comboEnd;
        if (comboEndTime < 1f)
        {
            var currentScale = this.ScoreDisplay.transform.localScale;
            currentScale.x = 1.0f + Easing.Pop(0.25f, 1.0f, comboEndTime);
            currentScale.y = 1.0f + Easing.Pop(0.25f, 1.0f, comboEndTime);
            this.ScoreDisplay.transform.localScale = currentScale;
        }
    }

    

    public bool AddPointEvent(string name, int value, GameObject originator)
    {
        if (this.currentCombo == null)
            this.currentCombo = new Combo();

        return this.currentCombo.AddPointEvent(new DestructionPointEvent(name, value, originator));
    }

    public bool AddPointEvent(string name, int value, GameObject originator, GameObject area)
    {
        if (this.currentCombo == null)
            this.currentCombo = new Combo();

        return this.currentCombo.AddPointEvent(new AreaPointEvent(name, value, originator, area));
    }

    public int GetScore()
    {
        return this.score;
    }
}
