﻿using UnityEngine;
using System.Collections;

public enum AudioType
{
    Aluminum,
    Cardboard
}

public class CollidableObject : MonoBehaviour
{
    public int PointValue = 10;

    public int BounceBonus = 10;

    public string ObjectName;

    public string PointEventName = "Smash";

    public string BounceEventName = "Bounce";

    public AudioType AudioType;

    private GameObject lastHit;

    private float lastSpeed;
    private float lastRotation;

	// Use this for initialization
	void Start ()
    {
        var body = this.GetComponent<Rigidbody>();
        lastSpeed = body.velocity.magnitude;
        lastRotation = body.angularVelocity.magnitude;
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void FixedUpdate()
    {
        var body = this.GetComponent<Rigidbody>();
        float speed = body.velocity.magnitude;
        var rotation = body.angularVelocity.magnitude;
        if (Mathf.Abs(this.lastSpeed - speed) > 10f || Mathf.Abs(this.lastRotation - rotation) > 15)
        {
            if (this.AudioType == AudioType.Aluminum)
                GameObject.FindObjectOfType<AudioManager>().PingAluminum();
            else
                GameObject.FindObjectOfType<AudioManager>().PingCardboard();

            if (this.lastHit.GetComponent<CollidableObject>() != null)
                GameObject.FindObjectOfType<ScoreManager>().AddPointEvent(string.Format("{2} {0} {1}", this.PointEventName, this.ObjectName, this.BounceEventName), this.PointValue + this.BounceBonus, this.gameObject);
            else
                GameObject.FindObjectOfType<ScoreManager>().AddPointEvent(string.Format("{0} {1}", this.PointEventName, this.ObjectName), this.PointValue, this.gameObject);
        }
        this.lastRotation = rotation;
        this.lastSpeed = speed;
    }

    void OnCollisionEnter(Collision collision)
    {
        this.lastHit = collision.collider.gameObject;
    }
}
