﻿using UnityEngine;
using System.Collections;

public class Smashable : MonoBehaviour
{
    public string Message;

    public int Value;

    public float ForceRequired;

    private bool smashed;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnCollisionEnter(Collision collider)
    {
        if (!this.smashed && collider.relativeVelocity.magnitude > this.ForceRequired)
        {
            var manager = GameObject.FindObjectOfType<ScoreManager>();
            manager.AddPointEvent(this.Message, this.Value, this.gameObject);
            this.smashed = true;
        }
    }
}
